import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class DataService {
  Future<WeatherParameters> getWeatherCity(String city) async{
    final parameters = {
      'q': city,
      'appid': '27f705a8238f610214a76b1dbd082ad6',
      'lang': 'tr',
    };
    final uri = Uri.https(
      'api.openweathermap.org', '/data/2.5/weather', parameters
    );
    final response = await http.get(uri);
    WeatherParameters.fromJsonForCity(jsonDecode(utf8.decode(response.bodyBytes)));
    return WeatherParameters.fromJsonForCity(jsonDecode(utf8.decode(response.bodyBytes)));
  }

  Future<WeatherParameters> getWeatherCoordinates(double latitude, double longitude) async{
    final parameters = {
      'lat': latitude.toString(),
      'lon': longitude.toString(),
      'appid': '27f705a8238f610214a76b1dbd082ad6',
      'lang': 'tr',
    };
    final uri = Uri.https(
        'api.openweathermap.org', '/data/2.5/onecall', parameters
    );
    final response = await http.get(uri);
    WeatherParameters.fromJsonForCoordinates(jsonDecode(utf8.decode(response.bodyBytes)));
    return WeatherParameters.fromJsonForCoordinates(jsonDecode(utf8.decode(response.bodyBytes)));
  }

  Future<WeatherForecast> getWeatherForecastCity(String city, int i) async{
    final parameters = {
      'q': city,
      'appid': '27f705a8238f610214a76b1dbd082ad6',
      'lang': 'tr',
    };
    final uri = Uri.https(
        'api.openweathermap.org', '/data/2.5/forecast', parameters
    );
    final response = await http.get(uri);
    return WeatherForecast.fromJson(jsonDecode(utf8.decode(response.bodyBytes)), i);
  }
}