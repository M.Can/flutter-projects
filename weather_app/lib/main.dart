import 'package:weather_flutter_app/data_service.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:weather_flutter_app/model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(
          title: 'Hava Durumu'
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final dataService = DataService();
  WeatherParameters wParameters = WeatherParameters("Konum bilgisi bekleniyor", "Konum bilgisi bekleniyor", 0, 0, "Konum bilgisi bekleniyor", "");
  WeatherForecast wForecast = WeatherForecast(0, "Konum bilgisi bekleniyor", "Konum bilgisi bekleniyor", "");
  WeatherForecastList wForecastList = WeatherForecastList([]);
  var items = ["Adana","Adıyaman","Afyon","Ağrı","Aksaray","Amasya","Ankara","Antalya","Ardahan","Artvin","Aydın","Balıkesir","Bartın","Batman","Bayburt","Bilecik","Bingol","Bitlis","Bolu","Burdur","Bursa","Çanakkale","Çankırı","Çorum","Denizli","Diyarbakır","Duzce","Edirne","Elazığ","Erzincan","Erzurum","Eskişehir","Gaziantep","Giresun","Gümüşhane","Hakkari","Hatay","Iğdır","Isparta","İstanbul","İzmir","Kahramanmaraş","Karabük","Karaman","Kars","Kastamonu","Kayseri","Kilis","Kırıkkale","Kırklareli","Kırşehir","Kocaeli","Konya","Kütahya","Malatya","Manisa","Mardin","Mersin","Muğla","Muş","Nevşehir","Niğde","Ordu","Osmaniye","Rize","Sakarya","Samsun","Şanlıurfa","Siirt","Sinop","Şırnak","Sivas","Tekirdağ","Tokat","Trabzon","Tunceli","Uşak","Van","Yalova","Yozgat","Zonguldak"];
  String? value;
  int forecastCount = 4;

  @override
  void initState() {
    super.initState();
    getLocation();
  }

  void getLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _locationData = await location.getLocation();
    getWeatherInfoCoordinate(_locationData.latitude!, _locationData.longitude!);
  }

  Future<void> getWeatherInfoCoordinate(double lat, double long) async {
    try {
      var weather = await dataService.getWeatherCoordinates(lat, long);
      setState(() {
        wParameters = weather;
      });
      getWeatherForecastListInfo(wParameters.cityName);
    } catch (err) {
      print(err);
    }
  }

  Future<void> getWeatherInfoCity(String cityName) async {
    try {
      setState(() {
        (value) => value!;
      });
      var weather = await dataService.getWeatherCity(cityName);
      setState(() {
        wParameters = weather;
      });
      getWeatherForecastListInfo(wParameters.cityName);
    } catch (err) {
      print(err);
    }
  }

  void getWeatherForecastListInfo(city) async{
    wForecastList.wForecastList.clear();
    try{
      int count = 8;
      for(var i = 0; i < forecastCount; i++){
        var weitherForecast = await dataService.getWeatherForecastCity(city, count);
        setState(() {
          wForecast = weitherForecast;
          wForecastList.wForecastList.add(wForecast);
        });
        count = count + 8;
      }
    }
    catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(widget.title),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          searchCity(),
          weather(),
          weatherForecast(),
        ],),
    );
  }

  Widget searchCity(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black54,
                  )
              ),
              child: Center(
                child: DropdownButton<String>(
                  underline: const SizedBox(),
                  value: value,
                  items: items.map(buildMenuItem).toList(),
                  onChanged: (String? newValue) {
                    setState(() {
                      value = newValue!;
                      getWeatherInfoCity(value!);
                    });
                  }),
              ),
            )
        )
      ],);
  }

  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
    value: item,
      child: Text(
        item,
      ),
  );

  Widget weather(){
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.location_on,
              ),
              Text(
                wParameters.cityName + " / " + wParameters.countryName,
              ),
            ],
          ),
          Text(
            wParameters.weatherDescription.trim().substring(0, 1).toUpperCase() + wParameters.weatherDescription.trim().substring(1),
            style: TextStyle(fontSize: 25),
          ),
          weatherIcon(wParameters.icon, 100),
          Text(
            wParameters.degree.toStringAsFixed(1) + " °C",
            style: TextStyle(fontSize: 50),
          ),
          Text(
            "Hissedilen: " + wParameters.degreeFeelsLike.toStringAsFixed(1) + " °C",
            style: TextStyle(fontSize: 15),
          ),
        ],),
    );
  }

  Widget weatherForecast(){
    return Padding(
      padding: const EdgeInsets.only(left:10, right:10, bottom: 5),
      child: weatherLoop(),
    );
  }

  Widget weatherLoop() {
    if(wForecastList.wForecastList.length == forecastCount){
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for(var i = 0; i < forecastCount; i++)
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Column(
                  children: [
                    SizedBox(
                      height: 50,
                      child: Center(
                        child: Text(
                          wForecastList.wForecastList[i].description.trim().substring(0, 1).toUpperCase() + wForecastList.wForecastList[i].description.trim().substring(1),
                        ),
                      ),
                    ),
                    weatherIcon(wForecastList.wForecastList[i].icon, 60),
                    Text(
                      wForecastList.wForecastList[i].degree.toStringAsFixed(1) + " °C",
                      style: TextStyle(fontSize: 18),
                    ),
                    Text(
                      wForecastList.wForecastList[i].date.substring(8, 10) + "/" + wForecastList.wForecastList[i].date.substring(5, 7) + "/" +wForecastList.wForecastList[i].date.substring(0, 4),
                    )
                  ],),
              ),
            )
        ],);
    }
    else if(wForecastList.wForecastList.isNotEmpty){
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for(var i = 0; i < forecastCount; i++)
            const Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: Center(
                    child: Text(
                      "Yükleniyor...",
                    ),
                  ),
                )
            )
        ],);
    }
    else{
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              "Konum bilgisi bekleniyor",
            ),
          ]);
    }
  }

  weatherIcon(String icon, double size){
    if(icon != ""){
      return Image.network(
        icon,
        height: size,
        width: size,
      );
    }
    else{
      return const Text(
        "Konum bilgisi bekleniyor",
      );
    }
  }
}
