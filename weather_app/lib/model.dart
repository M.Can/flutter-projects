class WeatherParameters {
  late String countryName;
  final String cityName;
  final double degree;
  final double degreeFeelsLike;
  final String weatherDescription;
  final String icon;

  WeatherParameters(
      this.countryName,
      this.cityName,
      this.degree,
      this.degreeFeelsLike,
      this.weatherDescription,
      this.icon
      );

  WeatherParameters.fromJsonForCity(Map<String, dynamic> json)
      : countryName = json['sys']['country'],
        cityName = json['name'].replaceAll("_", " "),
        degree = json['main']['temp'] - 273.15,
        degreeFeelsLike = json['main']['feels_like'] - 273.15,
        weatherDescription = json['weather'][0]['description'],
        icon = 'https://openweathermap.org/img/wn/' + json['weather'][0]['icon'] + '@2x.png';

  WeatherParameters.fromJsonForCoordinates(Map<String, dynamic> json)
      : countryName = json['timezone'].toString().split("/")[0],
        cityName = json['timezone'].toString().split("/")[1].replaceAll("_", " "),
        degree = json['current']['temp'] - 273.15,
        degreeFeelsLike = json['current']['feels_like'] - 273.15,
        weatherDescription = json['current']['weather'][0]['description'],
        icon = 'https://openweathermap.org/img/wn/' + json['current']['weather'][0]['icon'] + '@2x.png';
}

class WeatherForecastList {
  late List<WeatherForecast> wForecastList;

  WeatherForecastList(
      this.wForecastList,
      );
}

class WeatherForecast {
  final double degree;
  final String description;
  final String date;
  final String icon;

  WeatherForecast(
      this.degree,
      this.description,
      this.date,
      this.icon,
      );

  WeatherForecast.fromJson(Map<String, dynamic> json, int i)
      : degree = json['list'][i]['main']['temp'] - 273.15,
        description = json['list'][i]['weather'][0]['description'],
        date = json['list'][i]['dt_txt'],
        icon = 'https://openweathermap.org/img/wn/' + json['list'][i]['weather'][0]['icon'] + '@2x.png';
}