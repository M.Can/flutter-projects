import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class DataService {
  Future<BisimStations> getBisimStations() async{
    const String url = 'api.citybik.es';
    Uri uri = Uri.https(url, 'v2/networks/baksi-bisim');
    final response = await http.get(uri);
    Map<String, dynamic> obj = jsonDecode(utf8.decode(response.bodyBytes));
    List<dynamic> stations = obj['network']['stations'];
    List<BisimStation> bisimStations = [];
    for(int i = 0; i < stations.length; i++){
      bisimStations.add(BisimStation.fromJson(jsonDecode(utf8.decode(response.bodyBytes)), i));
    }
    return BisimStations.fromJson(bisimStations);
  }
}