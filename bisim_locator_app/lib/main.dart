import 'dart:async';
import 'package:flutter/material.dart';
import 'package:bisim_locator_app/data_service.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:location/location.dart';
import 'model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BİSİM Bul',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'BİSİM Bul'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CurrentLocation currentLoc = CurrentLocation(38.4237, 27.1428);
  final dataService = DataService();
  List<Marker> markers = <Marker>[];
  List<Polyline> polylines = <Polyline>[];
  late BisimStations bisimStations;
  BisimStation bisimStation = BisimStation('');
  final Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
    getLocation();
    getServiceRequest();
  }

  void getLocation() async {
    Location location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    _locationData = await location.getLocation();
    setState(() {
      currentLoc.latitude = _locationData.latitude!;
      currentLoc.longitude = _locationData.longitude!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          child:
            map(),
        ),
      ),
    );
  }

  Widget map(){
    return GoogleMap(
      mapType: MapType.normal,
      initialCameraPosition: CameraPosition(
        target: LatLng(currentLoc.latitude, currentLoc.longitude),
        zoom: 15,
      ),
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
      markers: markers.toSet(),
      polylines: polylines.toSet(),
    );
  }

  void markersCreate(){
    for(int i = 0; i < bisimStations.stations.length; i++){
      final marker = Marker(
        markerId: MarkerId(bisimStations.stations[i].istasyonAdi),
        position: LatLng(bisimStations.stations[i].latitude, bisimStations.stations[i].longitude),

        infoWindow: InfoWindow(
          onTap: () => onTapMarker(bisimStations.stations[i]),
          title: bisimStations.stations[i].istasyonAdi,
          snippet: 'Ayrıntılar için tıkla',
        ),
      );
      setState(() {
        markers.add(marker);
      });
    }
  }

  void getServiceRequest() async{
    try {
      var dataServiceBisim = await dataService.getBisimStations();
      setState(() {
        bisimStations = dataServiceBisim;
        markersCreate();
      });
    } catch(err){
      print(err);
    }
  }

  void onTapMarker(BisimStation bisimStationClick){
    setState(() {
      bisimStation = bisimStationClick;
    });
    showModalBottomSheet<void>(context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(bisimStation.istasyonAdi),
                Text('Bisiklet sayısı: ' + bisimStation.bisikletSayisi.toString()),
                Text('Park yeri sayısı: ' + bisimStation.parkYeri.toString()),
                ElevatedButton(
                  child: const Text('Yol tarifi'),
                  onPressed: createRoute,
                )
              ],
            ),
        );
      }
    );
  }

  void createRoute() async{
    Navigator.pop(context);
    PolylineResult result = await PolylinePoints().getRouteBetweenCoordinates(
      "AIzaSyCO86dV6UF3id8HBgBynGdIIhKXs26NFkw",
      PointLatLng(currentLoc.latitude, currentLoc.longitude),
      PointLatLng(bisimStation.latitude, bisimStation.longitude),
      travelMode: TravelMode.transit,
    );
    print(result.errorMessage! + ' ' + result.status!);
    if(result.points.isNotEmpty){
      List<LatLng> listLatLng = [];
      for(int i = 0; i < result.points.length; i++){
        listLatLng.add(LatLng(result.points[i].latitude, result.points[i].longitude));
      }
      Polyline polyline = Polyline(
        polylineId: const PolylineId('polyline'),
        points: listLatLng,
      );
      setState(() {
        polylines.add(polyline);
      });
    }
    else{

    }
  }
}
