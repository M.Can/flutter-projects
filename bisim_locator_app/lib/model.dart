class CurrentLocation{
  late double latitude;
  late double longitude;

  CurrentLocation(
      this.latitude,
      this.longitude
      );
}

class BisimStations{
  late List<BisimStation> stations;

  BisimStations.fromJson(List<BisimStation> listFromJson)
      : stations = listFromJson;
}

class BisimStation{
  late String istasyonAdi;
  late int bisikletSayisi;
  late int parkYeri;
  late double latitude;
  late double longitude;

  BisimStation(
      this.istasyonAdi,
      );

  BisimStation.fromJson(Map<String, dynamic> json, int i)
      : istasyonAdi = json['network']['stations'][i]['name'],
        bisikletSayisi = json['network']['stations'][i]['free_bikes'],
        parkYeri = json['network']['stations'][i]['empty_slots'],
        latitude = json['network']['stations'][i]['latitude'],
        longitude = json['network']['stations'][i]['longitude'];
}