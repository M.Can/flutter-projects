import 'package:flutter/material.dart';
import 'package:lyrics_finder_app/data_service.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:lyrics_finder_app/model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Şarkı Sözü Bul',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Şarkı Sözü Bul'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final dataService = DataService();
  Lyrics lyrics = Lyrics('');
  late String videoUrl = '';
  late String artist = '';
  late String song = '';

  void getLyrics() async{
    try {
      var dataServiceLyrics = await dataService.getLyrics(artist, song);
      var dataServiceVideos = await dataService.getVideosFromYoutube(artist, song);
      setState(() {
        lyrics = dataServiceLyrics;
        videoUrl = dataServiceVideos;
      });
    } catch(err){
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: [
            inputArea(),
            videoArea(),
            lyricsArea(),
            ],
        ),
      ),
    );
  }

  Widget inputArea(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Şarkıcı adını yazınız',
              ),
              textInputAction: TextInputAction.next,
              onChanged: (value) {
                setState(() {
                  artist = value;
                });
              },
            ),
            const SizedBox(
              height: 15,
            ),
            TextField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Şarkı adını yazınız',
              ),
              textInputAction: TextInputAction.search,
              onSubmitted: (value) {
                setState(() {
                  song = value;
                });
                getLyrics();
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget videoArea(){
    if(videoUrl.isNotEmpty){
      return Container(
        height: 200,
        child: InAppWebView(
          key: GlobalKey(),
          initialUrlRequest: URLRequest(url: Uri.parse(videoUrl)),
        )
      );
    }
    else{
      return Container(
        height: 0,
      );
    }
  }

  Widget lyricsArea(){
    if(lyrics.lyrics.isNotEmpty){
      return SizedBox(
        child: DraggableScrollableSheet(
          initialChildSize: 0.1,
          minChildSize: 0.1,
          maxChildSize: 0.8,
          builder: (BuildContext context, ScrollController scrollController) {
            return Container(
              color: Colors.blue[100],
              child: ListView.builder(
                controller: scrollController,
                itemCount: 1,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(lyrics.lyrics),
                  );
                },
              ),
            );
          },
        ),
      );
    }
    else{
      return const SizedBox(
          height: 0,
      );
    }
  }
}
