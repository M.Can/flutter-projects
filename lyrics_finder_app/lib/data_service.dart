import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class DataService {
  Future<Lyrics> getLyrics(String artist, String song) async{
    const String url = 'api.lyrics.ovh';
    Uri uri = Uri.https(url, 'v1/' + artist + '/' + song);
    print(uri);
    final response = await http.get(uri);
    Lyrics.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    return Lyrics.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
  }

  Future<String> getVideosFromYoutube(String artist, String song) async{
    const String url = 'youtube.com';
    String queryStr1 = (artist + song).replaceAll(' ', '+');
    Map<String, String> headers = {
      "search_query": queryStr1,
    };
    Uri uri = Uri.https(url, 'results', headers);
    print(uri);
    return uri.toString();
  }
}