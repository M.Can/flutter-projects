import 'package:flutter/material.dart';
import 'package:tr_covid_statistics/data_service.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('tr'),
      ],
      locale: const Locale('tr'),
      title: 'Türkiye Covid İstatistiği',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Türkiye Covid İstatistiği'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isPlay = false;
  final dataService = DataService();
  DateTime yesterday = DateTime.now().subtract(const Duration(days: 1));
  late String soundSentence;
  CovidStatistics statistics = CovidStatistics(0, 0, 0, 0, 0, 0, DateTime.now().subtract(const Duration(days: 1)).toString().substring(0, 10));
  FlutterTts flutterTts = FlutterTts();

  @override
  void initState() {
    super.initState();
    getYesterdayDate();
  }

  void getYesterdayDate() async{
    try {
      String yesterdaySrt = yesterday.toString().substring(0, 10);
      var dataServiceStatistics = await dataService.getStatistics(yesterdaySrt);
      setState(() {
        statistics = dataServiceStatistics;
        soundSentence = 'günlük vaka sayısı ' + statistics.confirmed_diff.toString() + ' günlük  vefat sayısı' + statistics.deaths_diff.toString() + ' toplam vaka sayısı ' + statistics.confirmed.toString() + ' toplam vefat sayısı ' + statistics.deaths.toString();
      });
    } catch(err){
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(top: 50.0, bottom: 30.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                showStatistics(),
                voiceButton(),
                datePicker(),
            ]),
          ),
        ),
      ),
    );
  }

  Widget showStatistics(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Table(
            border: TableBorder.all(color: const Color.fromRGBO(15, 75, 75, 1.0)),
            defaultColumnWidth: const FixedColumnWidth(160.0),
            children: [
              TableRow(
                  children: [
                  Container(
                      decoration: const BoxDecoration(color: Color.fromRGBO(27, 135, 139, 1.0)),
                      child: const Center(
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Text('Tarih',style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                          )
                      )
                  ),
                  Center(child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(statistics.date),
                  ))
                ]
              ),
              TableRow(
                  children: [
                    Container(
                        decoration: const BoxDecoration(color: Color.fromRGBO(27, 135, 139, 1.0)),
                        child: const Center(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text('Vaka Sayısı',style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            )
                        )
                    ),
                    Center(child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(statistics.confirmed_diff.toString()),
                    ))
                  ]
              ),
              TableRow(
                  children: [
                    Container(
                        decoration: const BoxDecoration(color: Color.fromRGBO(27, 135, 139, 1.0)),
                        child: const Center(
                              child: Padding(
                                padding: EdgeInsets.all(5.0),
                                child: Text('Vefat Sayısı',style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                              )
                          ),
                    ),
                    Center(child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(statistics.deaths_diff.toString()),
                    ))
                  ]
              ),TableRow(
                  children: [
                    Container(
                        decoration: const BoxDecoration(color: Color.fromRGBO(27, 135, 139, 1.0)),
                        child: const Center(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text('Toplam Vaka Sayısı',style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            )
                        )
                    ),
                    Center(child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(statistics.confirmed.toString()),
                    ))
                  ]
              ),TableRow(
                  children: [
                    Container(
                        decoration: const BoxDecoration(color: Color.fromRGBO(27, 135, 139, 1.0)),
                        child: const Center(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Text('Toplam Vefat Sayısı',style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                            )
                        )
                    ),
                    Center(child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(statistics.deaths.toString()),
                    ))]
              )],
          ),
        ),],
    );
  }

  Widget voiceButton(){
    if(isPlay){
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: const Icon(Icons.stop),
            onPressed: pressVoiceButton,
          ),],
      );
    }
    else{
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: const Icon(Icons.volume_up),
            onPressed: pressVoiceButton,
          ),],
      );
    }
  }

  Widget datePicker(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SfDateRangePicker(
          onSelectionChanged: onSelectionChanged,
          selectionMode: DateRangePickerSelectionMode.single,
          maxDate: yesterday,
          minDate: DateTime.utc(2019),
          showNavigationArrow: true,
        ),],
    );
  }

  Future pressVoiceButton() async{
    if(!isPlay){
      setState(() {
        isPlay = true;
      });
      await flutterTts.setLanguage('tr-TR');
      await flutterTts.setVolume(1.0);
      await flutterTts.setPitch(1);
      await flutterTts.speak(soundSentence);
      flutterTts.setCompletionHandler(() {
        setState(() {
          isPlay = false;
        });
      });
    }
    else{
      setState(() {
        isPlay = false;
      });
      await flutterTts.stop();
    }
  }

  void onSelectionChanged(DateRangePickerSelectionChangedArgs args) async{
    await flutterTts.stop();
    setState(() {
      isPlay = false;
    });
    var dataServiceStatistics = await dataService.getStatistics(args.value.toString().substring(0, 10));
    setState(() {
      statistics = dataServiceStatistics;
      soundSentence = 'günlük vaka sayısı ' + statistics.confirmed_diff.toString() + ' günlük  vefat sayısı' + statistics.deaths_diff.toString() + ' toplam vaka sayısı ' + statistics.confirmed.toString() + ' toplam vefat sayısı ' + statistics.deaths.toString();
    });
  }
}
