import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class DataService {
  static const String api_key = 'e186f8534emshc98d7a644132324p1c1d58jsn55e8b96510e9';

  Future<CovidStatistics> getStatistics(String date) async{
    const String url = 'covid-19-statistics.p.rapidapi.com';
    Map<String, String> parameters = {
      'iso': 'TUR',
      'date': date,
    };
    Uri uri = Uri.https(url, 'reports', parameters);
    const Map<String, String> headers = {
      "x-rapidapi-host": url,
      "x-rapidapi-key": api_key,
    };
    print(uri);
    final response = await http.get(uri, headers: headers);
    CovidStatistics.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    return CovidStatistics.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
  }
}