class CovidStatistics{
  late int active;
  late int confirmed;
  late int deaths;
  late int active_diff;
  late int confirmed_diff;
  late int deaths_diff;
  final String date;

  CovidStatistics(
      this.active,
      this.confirmed,
      this.deaths,
      this.active_diff,
      this.confirmed_diff,
      this.deaths_diff,
      this.date,
      );

  CovidStatistics.fromJson(Map<String, dynamic> json)
      : active = json['data'][0]['active'],
        confirmed = json['data'][0]['confirmed'],
        deaths = json['data'][0]['deaths'],
        active_diff = json['data'][0]['active_diff'],
        confirmed_diff  = json['data'][0]['confirmed_diff'],
        deaths_diff = json['data'][0]['deaths_diff'],
        date = json['data'][0]['date'];
}