import 'dart:convert';

class CreditCardDetail{
  final String country;
  final String countryCode;
  final dynamic cardBrand;
  final String issuer;
  final String issuerPhone;
  final String cardType;

  CreditCardDetail(
      this.country,
      this.countryCode,
      this.cardBrand,
      this.issuer,
      this.issuerPhone,
      this.cardType
      );

  CreditCardDetail.fromJson(Map<String, dynamic> json)
      : country = json['country']['name'],
        countryCode = json['country']['alpha2'],
        cardBrand = json['brand'],
        issuer = jsonEncode(json['bank']).contains('name') == true ? json['bank']['name']: 'Banka bilgisi gelmedi',
        issuerPhone = jsonEncode(json['bank']).contains('phone') == true ? json['bank']['phone']: 'Banka bilgisi gelmedi',
        cardType = json['type'];
}