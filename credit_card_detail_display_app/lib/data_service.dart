import 'dart:convert';
import 'package:http/http.dart' as http;
import 'model.dart';

class DataService {
  static const String api_key = 'e186f8534emshc98d7a644132324p1c1d58jsn55e8b96510e9';

  Future<CreditCardDetail> getSCardDetails(int cardNumber) async{
    const String url = 'lookup.binlist.net';
    Map<String, String> parameters = {
      'bin-number': cardNumber.toString(),
      'customer-ip': ''
    };
    Uri uri = Uri.https(url, cardNumber.toString());
    const Map<String, String> headers = {
      "x-rapidapi-host": url,
      "x-rapidapi-key": api_key,
    };
    print(uri);
    final response = await http.get(uri);
    if(response.statusCode == 200){
      print(utf8.decode(response.bodyBytes));
    }
    else{
      print(response.statusCode);
    }
    CreditCardDetail.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    return CreditCardDetail.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
  }
}