import 'package:credit_card_detail_display_app/model.dart';
import 'package:flutter/material.dart';
import 'package:credit_card_detail_display_app/data_service.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kredi Kartı Detayı',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Kredi Kartı Detayı'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final dataService = DataService();
  CreditCardDetail cardDetail = CreditCardDetail("", "", "", "", "", "");

  @override
  void initState() {
    super.initState();
  }

  void getCardDetail(int cardNumber) async{
    try {
      var dataServiceCardDetail = await dataService.getSCardDetails(cardNumber);
      setState(() {
        cardDetail = dataServiceCardDetail;
      });
    } catch(err){
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            inputCardNumber(),
            cardDetailArea(),
          ],
        ),
      ),
    );
  }

  Widget inputCardNumber(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: TextField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: "Kartınızın ilk 6 rakamını giriniz"
        ),
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ],
        maxLength: 6,
        textInputAction: TextInputAction.search,
        onSubmitted: (value){
          getCardDetail(int.parse(value));
        },
      ),
    );
  }

  Widget cardDetailArea(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Ülke: ' + cardDetail.country + "\n" +
            'Ülke kodu: ' + cardDetail.countryCode + "\n" +
            'Kart markası: ' + cardDetail.cardBrand + "\n" +
            'Banka: ' + cardDetail.issuer + "\n" +
            'Banka tel: ' + cardDetail.issuerPhone + "\n" +
            'Kart tipi: ' + cardDetail.cardType + "\n"
        ),
      ],
    );
  }
}
